package com.steeplesoft.kotlinee

import com.steeplesoft.kotlinee.service.MessageService
import com.steeplesoft.kotlinee.service.UserService
import org.jboss.arquillian.container.test.api.Deployment
import org.jboss.arquillian.junit.Arquillian
import org.jboss.shrinkwrap.api.spec.WebArchive
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

/**
 * @author jdlee
 * @since 2015-11-10
 */
@RunWith(Arquillian::class)
class UserServiceTest  : AbstractBaseTest() {
    @PersistenceContext(unitName = "em")
    private lateinit var em : EntityManager


    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var messageService: MessageService

    companion object {
        @JvmStatic
        @Deployment
        fun createDeployment(): WebArchive {
            return AbstractBaseTest.createBaseDeployment()
        }
    }

    @Before
    fun setup() {
    }

    @Test
    fun readUser() {
        val user = userService.getUser(2)
        println(user)
    }

    @Test
    fun testUserCrud() : Unit {
        val user = userService.createUser("user", "email")
        Assert.assertNotNull(user)
        Assert.assertNotNull(user.id)

        Assert.assertEquals(user, userService.getUser(user.id))

        val users = userService.getUsers()
        Assert.assertNotNull(users)
        Assert.assertNotSame(0, users.size)

        user.name = "user edited"
        userService.updateUser(user)
        val user2 = userService.getUser(user.id)
        Assert.assertEquals("user edited", user2?.name)
        Assert.assertEquals(user, user2)

        userService.deleteUser(user);
        Assert.assertNull(userService.getUser(user.id))
    }
}