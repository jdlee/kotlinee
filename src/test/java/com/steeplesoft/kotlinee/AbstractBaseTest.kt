package com.steeplesoft.kotlinee

import org.jboss.shrinkwrap.api.ShrinkWrap
import org.jboss.shrinkwrap.api.spec.JavaArchive
import org.jboss.shrinkwrap.api.spec.WebArchive
import org.jboss.shrinkwrap.resolver.api.maven.Maven
import java.io.File

/**
 * @author jdlee
 * @since 2015-11-06
 */
abstract class AbstractBaseTest {
    companion object {
        @JvmStatic
        fun createBaseDeployment(): WebArchive {
            val kotlinRuntime = Maven.configureResolver()
                    .workOffline()
                    .withMavenCentralRepo(true)
                    .withClassPathResolution(true)
                    .loadPomFromFile("pom.xml")
                    .resolve("org.jetbrains.kotlin:kotlin-stdlib").withTransitivity().`as`(JavaArchive::class.java)
            return ShrinkWrap.create(WebArchive::class.java, "test.war")
                    .addPackages(true, "com.steeplesoft.kotlinee")
                    .addAsWebInfResource(File("src/main/webapp/WEB-INF/web.xml"))
                    .addAsManifestResource(File("src/main/webapp/WEB-INF/beans.xml"))
                    .addAsResource("META-INF/persistence.xml")
                    .addAsLibraries(kotlinRuntime)
        }
    }
}