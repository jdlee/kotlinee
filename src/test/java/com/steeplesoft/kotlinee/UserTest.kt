package com.steeplesoft.kotlinee

import com.steeplesoft.kotlinee.model.User
import org.jboss.arquillian.container.test.api.Deployment
import org.jboss.arquillian.junit.Arquillian
import org.jboss.shrinkwrap.api.spec.WebArchive
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.UserTransaction

/**
 * @author jdlee
 * @since 2015-11-12
 */
@RunWith(Arquillian::class)
class UserTest : AbstractBaseTest() {
    @PersistenceContext(unitName = "em")
    private lateinit var em: EntityManager

    @Inject
    private lateinit var utx: UserTransaction

    companion object {
        @JvmStatic
        @Deployment
        fun createDeployment(): WebArchive {
            return AbstractBaseTest.createBaseDeployment()
        }
    }

    @Test
    fun userCrud() {
        startTransaction()

        val user = User("user1", "user@example.com")
        em.persist(user)
        utx.commit()

        var user1 = em.find(User::class.java, user.id)
        Assert.assertNotNull(user1)
        Assert.assertEquals(user, user1)

        startTransaction()
        user.name = "user1 - edited"
        em.merge(user)
        utx.commit()

        user1 = em.find(User::class.java, user.id)
        Assert.assertNotNull(user1)
        Assert.assertEquals(user, user1)

        startTransaction()
        em.remove(em.find(User::class.java, user.id))
        utx.commit()
        Assert.assertNull(em.find(User::class.java, user.id))
    }

    private fun startTransaction() {
        utx.begin();
        em.joinTransaction();
    }
}