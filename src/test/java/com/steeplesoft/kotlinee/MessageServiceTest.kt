package com.steeplesoft.kotlinee

import com.steeplesoft.kotlinee.model.User
import com.steeplesoft.kotlinee.service.MessageService
import com.steeplesoft.kotlinee.service.UserService
import org.jboss.arquillian.container.test.api.Deployment
import org.jboss.arquillian.junit.Arquillian
import org.jboss.shrinkwrap.api.spec.WebArchive
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

/**
 * @author jdlee
 * @since 2015-11-10
 */
@RunWith(Arquillian::class)
class MessageServiceTest  : AbstractBaseTest() {
    @Inject
    lateinit var service : MessageService
    @Inject
    lateinit var userService : UserService

    companion object {
        @JvmStatic
        @Deployment
        fun createDeployment(): WebArchive {
            return AbstractBaseTest.createBaseDeployment()
        }
    }

    @Test
    fun testMessageCrud() {
        val user = userService.createUser("messageTest", "messageTest@example.com")
        val message = service.createMessage(user, "test message")
        Assert.assertNotNull(message)
        Assert.assertNotNull(message.user)
        Assert.assertEquals(user.id, message.user?.id)
        println(message)

        val messages = service.getMessages()
        Assert.assertNotSame(0, messages.size)

        service.deleteMessage(message.id!!) // ?: -1)

        Assert.assertNull(service.getMessage(message.id!!))
        userService.deleteUser(user)
    }

    @Test
    fun testGetNonExistentMessage() {
        val message = service.getMessage(-1)
        Assert.assertNull(message) // And doesn't blow up :)
    }

    @Test
    fun deleteNonExistentMessage() {
        service.deleteMessage(-1)
        println("hi")
    }
}