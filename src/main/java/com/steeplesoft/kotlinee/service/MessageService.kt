package com.steeplesoft.kotlinee.service

import com.steeplesoft.kotlinee.model.Message
import com.steeplesoft.kotlinee.model.User
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

/**
 * @author jdlee
 * @since 2015-11-10
 */
interface MessageService {
    fun getMessages(seconds: Long = 90): List<Message>
    fun getMessage(id: Long): Message?
    fun createMessage(user: User, text: String): Message
    fun updateMessage(message: Message)
    fun deleteMessage(id: Long)
}

@RequestScoped
open class MessageServiceImpl : MessageService {
    @PersistenceContext(unitName = "em")
    private lateinit var em: EntityManager

    @Inject
    private lateinit var userService: UserService

    override fun getMessages(seconds: Long): List<Message> {
        val query = em.createQuery("SELECT m FROM Message m ORDER BY m.messageDate", Message::class.java)
        //        query.setParameter("age", System.currentTimeMillis() - (seconds * 1000))
        return query.resultList
    }

    override fun getMessage(id: Long): Message? {
        return em.find(Message::class.java, id)
    }

    @Transactional
    override fun createMessage(user: User, text: String): Message {
        val user1 = userService.getUser(user.id)
        em.refresh(user1)
        val message = Message(text, user1)
        em.persist(message)

        return message
    }

    @Transactional
    override fun updateMessage(message: Message) {
        em.refresh(message)
    }

    @Transactional
    override fun deleteMessage(id: Long) {
        val toDelete: Message? = em.find(Message::class.java, id)
        toDelete?.let {
            em.remove(toDelete)
        }
    }
}