package com.steeplesoft.kotlinee.service

import com.steeplesoft.kotlinee.model.User
import javax.enterprise.context.RequestScoped
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

/**
 * @author jdlee
 * @since 2015-11-10
 */
interface UserService {
    fun getUsers() : List<User>
    fun getUser(id : Long?) : User?
    fun createUser(name : String, email : String) : User
    fun updateUser(user : User)
    fun deleteUser(user: User)
    fun deleteUser(user : Number?)
}

@RequestScoped
open class UserServiceImpl : UserService {
    @PersistenceContext(unitName = "em")
    private lateinit var em : EntityManager

    override fun getUsers(): List<User> {
        return em.createQuery("SELECT u From User u ORDER BY u.name", User::class.java).resultList
    }

    override fun getUser(id: Long?) : User? {
        return em.find(User::class.java, id)
    }

    @Transactional
    override fun createUser(name: String, email: String): User {
        val user = User(name, email)
        em.persist(user)

        return user
    }

    @Transactional
    override fun deleteUser(user: User) {
        deleteUser(user.id)
    }

    @Transactional
    override fun deleteUser(user: Number?) {
        val toDelete = em.find(User::class.java, user)
        toDelete?.let {
            em.remove(toDelete)
        }
    }

    @Transactional
    override fun updateUser(user: User) {
        em.merge(user)
    }
}