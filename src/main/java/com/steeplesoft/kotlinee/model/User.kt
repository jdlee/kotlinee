package com.steeplesoft.kotlinee.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * @author jdlee
 * @since 2015-11-10
 */
@Entity
@Table(name="users")
data class User(@Column var name : String?,
                @Column var email : String?) {
    constructor() : this(null, null)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Long? = null

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    val messages : List<Message>? = null
}