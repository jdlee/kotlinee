package com.steeplesoft.kotlinee.model

import java.util.*
import javax.persistence.*

/**
 * @author jdlee
 * @since 2015-11-10
 */
@Entity
@Table(name="messages")
data class Message(@Column
                   var message: String?,

                   @ManyToOne(fetch=javax.persistence.FetchType.EAGER)
                   @JoinColumn(name="user_id")
                   var user : User?) {
    constructor() : this(null, null)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Long? = null

    @Temporal(value= TemporalType.TIMESTAMP)
    @Column
    var messageDate : Date = Date()

}