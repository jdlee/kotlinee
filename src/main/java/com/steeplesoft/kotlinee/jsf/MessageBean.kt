package com.steeplesoft.kotlinee.jsf

import com.steeplesoft.kotlinee.model.Message
import com.steeplesoft.kotlinee.model.User
import com.steeplesoft.kotlinee.service.MessageService
import com.steeplesoft.kotlinee.service.UserService
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.inject.Named
import javax.transaction.Transactional

/**
 * @author jdlee
 * @since 2015-11-15
 */

interface MessageBean {
    fun getMessages() : List<Message>
    fun saveMessage() : String
}

@Named(value="messageBean")
@RequestScoped
open class MessageBeanImpl : MessageBean {
    @Inject
    private lateinit var userService : UserService
    @Inject
    private lateinit var messageService : MessageService

    var messageText : String = ""

    override fun getMessages() : List<Message> {
        return messageService.getMessages()
    }

    @Transactional
    override fun saveMessage() : String {
        val user = userService.getUser(13)
        user?.let {
            messageService.createMessage(user, messageText)
        }
        return "index?faces-redirect=true";
    }
}