# KotlinEE #

KotlinEE is a small, but intentionally over-engineered attempt to use various Java EE APIs in a Kotlin-based
application. The application is a very simple Twitter-like clone. Messages can posted, users mentioned, and
notifications emailed. It's probably not very secure and certainly won't scale, but the purpose is to 
demonstrate the interoperability of Kotlin with Java APIs.

### Building ###

KotlinEE is a Maven-based project, so building the application is very straightforward. However, the 
application is tested using Arquillian, which makes use of an external resource, namely PostgreSQL.
Before building, then, PostgreSQL will need to be installed (which is beyond the scope of this document)
and configured:

```
$ ./db.sh
$ mvn clean install
```

Once the build is done, you should have a deployable archive in `target/`.
