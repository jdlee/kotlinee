#!/bin/bash

asadmin delete-jdbc-resource jdbc/kotlinee &>/dev/null
asadmin delete-jdbc-connection-pool KotlinEEPool &>/dev/null

asadmin create-jdbc-connection-pool \
    --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
    --restype javax.sql.ConnectionPoolDataSource \
    --property User=kotlinee:Password=kotlinee:DatabaseName=kotlinee:ServerName=localhost \
    KotlinEEPool #&> /dev/null

asadmin create-jdbc-resource --connectionpoolid KotlinEEPool jdbc/kotlinee #&> /dev/null

dropdb --if-exists kotlinee 2>/dev/null
dropuser --if-exists kotlinee 2>/dev/null
createuser kotlinee
createdb kotlinee -O kotlinee

psql -U kotlinee kotlinee << EOF
    DROP TABLE IF EXISTS messages;
    DROP TABLE IF EXISTS users;
    CREATE TABLE users (id serial primary key, name text, email text);
    CREATE TABLE messages (id serial primary key, message text, messageDate timestamp with time zone, user_id int REFERENCES users);
    CREATE TABLE books (id serial primary key, name text, description text, isbn text);
EOF
